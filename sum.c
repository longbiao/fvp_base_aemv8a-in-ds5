#include <stdio.h>
#define ITERATIONS 8192
float values[ITERATIONS];
void fill()
{
  for (int i = 0; i < ITERATIONS; i++)
  {
    values[i] = (float)i;
  }
}
 
float reduce() {
  float result = 0.0;
  for (int i = 0; i < ITERATIONS; i++)
  {
    result += values[i];
  }
  return result;
}
 
int main(int argc, char* argv[]) {
  printf("fill start!\n");
  fill();
  printf("reduce start!\n");
  printf("Result was %f\n", reduce());
}
