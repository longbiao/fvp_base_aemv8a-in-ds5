	.text
	.file	"sum.c"
	.section	.text.fill,"ax",@progbits
	.hidden	fill                    // -- Begin function fill
	.globl	fill
	.p2align	2
	.type	fill,@function
fill:                                   // @fill
	.cfi_startproc
// BB#0:
.Lcfi0:
	.cfi_def_cfa_offset 0
	orr	w9, wzr, #0x2000
	adrp	x10, values
	cntw	x11
	mov	x8, xzr
	index	z1.s, #0, #1
	add	x10, x10, :lo12:values
	whilelo	p1.s, xzr, x9
	mov	z0.s, w11
	ptrue	p0.s
.LBB0_1:                                // =>This Inner Loop Header: Depth=1
	scvtf	z2.s, p0/m, z1.s
	st1w	{z2.s}, p1, [x10, x8, lsl #2]
	incw	x8
	add	z1.s, z1.s, z0.s
	whilelo	p1.s, x8, x9
	b.mi	.LBB0_1
// BB#2:
	ret
.Lfunc_end0:
	.size	fill, .Lfunc_end0-fill
	.cfi_endproc
                                        // -- End function
	.section	.text.reduce,"ax",@progbits
	.hidden	reduce                  // -- Begin function reduce
	.globl	reduce
	.p2align	2
	.type	reduce,@function
reduce:                                 // @reduce
	.cfi_startproc
// BB#0:
.Lcfi1:
	.cfi_def_cfa_offset 0
	orr	w9, wzr, #0x2000
	adrp	x10, values
	mov	x8, xzr
	fmov	s0, wzr
	whilelo	p0.s, xzr, x9
	add	x10, x10, :lo12:values
.LBB1_1:                                // =>This Inner Loop Header: Depth=1
	ld1w	{z1.s}, p0/z, [x10, x8, lsl #2]
	incw	x8
	fadda	s0, p0, s0, z1.s
	whilelo	p0.s, x8, x9
	b.mi	.LBB1_1
// BB#2:
	ret
.Lfunc_end1:
	.size	reduce, .Lfunc_end1-reduce
	.cfi_endproc
                                        // -- End function
	.section	.text.main,"ax",@progbits
	.hidden	main                    // -- Begin function main
	.globl	main
	.p2align	2
	.type	main,@function
main:                                   // @main
	.cfi_startproc
// BB#0:
	addvl	sp, sp, #-1
	str	x28, [sp, #-32]!        // 8-byte Folded Spill
	stp	x19, x30, [sp, #16]     // 8-byte Folded Spill
.Lcfi2:                                 // cfa = SP + 32 + VG * 8
	.cfi_escape 0x0f, 0x0d, 0x11, 0x08, 0x92, 0x2e, 0x00, 0x1e, 0x11, 0x20, 0x22, 0x92, 0x1f, 0x00, 0x22
.Lcfi3:                                 // cfi(LR) = SP + 24
	.cfi_escape 0x10, 0x1e, 0x06, 0x11, 0x18, 0x92, 0x1f, 0x00, 0x22
.Lcfi4:                                 // cfi(X19) = SP + 16
	.cfi_escape 0x10, 0x13, 0x06, 0x11, 0x10, 0x92, 0x1f, 0x00, 0x22
.Lcfi5:                                 // cfi(X28) = SP + 0
	.cfi_escape 0x10, 0x1c, 0x06, 0x11, 0x00, 0x92, 0x1f, 0x00, 0x22
	adrp	x0, .Lstr
	add	x0, x0, :lo12:.Lstr
	bl	puts
	orr	w9, wzr, #0x2000
	adrp	x19, values
	cntw	x10
	mov	x8, xzr
	index	z1.s, #0, #1
	add	x19, x19, :lo12:values
	whilelo	p1.s, xzr, x9
	mov	z0.s, w10
	ptrue	p0.s
	add	x10, sp, #32            // =32
	str	p1, [x10]               // 2-byte Folded Spill
.LBB2_1:                                // =>This Inner Loop Header: Depth=1
	scvtf	z2.s, p0/m, z1.s
	st1w	{z2.s}, p1, [x19, x8, lsl #2]
	incw	x8
	add	z1.s, z1.s, z0.s
	whilelo	p1.s, x8, x9
	b.mi	.LBB2_1
// BB#2:
	adrp	x0, .Lstr.3
	add	x0, x0, :lo12:.Lstr.3
	bl	puts
	add	x10, sp, #32            // =32
	ldr	p0, [x10]               // 2-byte Folded Reload
	mov	x8, xzr
	fmov	s0, wzr
	orr	w9, wzr, #0x2000
.LBB2_3:                                // =>This Inner Loop Header: Depth=1
	ld1w	{z1.s}, p0/z, [x19, x8, lsl #2]
	incw	x8
	fadda	s0, p0, s0, z1.s
	whilelo	p0.s, x8, x9
	b.mi	.LBB2_3
// BB#4:
	adrp	x0, .L.str.2
	fcvt	d0, s0
	add	x0, x0, :lo12:.L.str.2
	bl	__2printf
	ldp	x19, x30, [sp, #16]     // 8-byte Folded Reload
	mov	w0, wzr
	ldr	x28, [sp], #32          // 8-byte Folded Reload
	addvl	sp, sp, #1
	ret
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc
                                        // -- End function
	.hidden	values                  // @values
	.type	values,@object
	.section	.bss.values,"aw",@nobits
	.globl	values
	.p2align	2
values:
	.zero	32768
	.size	values, 32768

	.type	.L.str.2,@object        // @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Result was %f\n"
	.size	.L.str.2, 15

	.type	.Lstr,@object           // @str
	.section	.rodata.str1.4,"aMS",@progbits,1
	.p2align	2
.Lstr:
	.asciz	"fill start!"
	.size	.Lstr, 12

	.type	.Lstr.3,@object         // @str.3
	.p2align	2
.Lstr.3:
	.asciz	"reduce start!"
	.size	.Lstr.3, 14

	.type	__ARM_use_no_argv,@object // @__ARM_use_no_argv
	.section	.ARM.use_no_argv,"aw",@progbits
	.globl	__ARM_use_no_argv
	.p2align	2
__ARM_use_no_argv:
	.word	1                       // 0x1
	.size	__ARM_use_no_argv, 4

	.globl	__ARM_use_no_argv
	.globl	_printf_percent
	.globl	_printf_f
	.globl	_printf_fp_dec
	.hidden	_printf_percent
	.hidden	_printf_f
	.hidden	_printf_fp_dec

	.ident	"Component: ARM Compiler 6.10.1 Tool: armclang [5ced1200]"
	.section	".note.GNU-stack","",@progbits
"BuildAttributes$$A64_ISAv8$P$Z$N$DE$K$PE$MPE$~IW$USESV6$~STKCKD$USESV7$~SHL$OSPACE$EBA8$REQ8$PRES8$EABIv2$WCHAR32$ENUMINT$A:L22UL41UL21$X:L11$S22US41US21$FP_ARMv8$IEEE1" = 0
