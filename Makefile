override CFLAGS+=-march=armv8-a+sve --target=aarch64-arm-none-eabi -O3 -Xlinker "--ro_base=0x80000000"
sum.elf : sum.c
	armclang -o sum.elf sum.c $(CFLAGS)
	armclang -S -o sum.s sum.c $(CFLAGS)
