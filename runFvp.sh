#!/bin/bash
# fvp-run.sh
# Usage: fvp-run.sh [veclen] [binary]
#    Executes the specified binary in the FVP, with no command-line
#    arguments.  The SVE register width will be [veclen] x 64 bits. Only
#    even values of veclen are valid.
#
#
# Set the FVP_BASE environment variable to point to the FVP directory.
#
# Set the ARMLMD_LICENSE_FILE environment variable to reference a license    
# file or license server with entitlement for the FVP in DS-5!.
 
VECLEN=$1
CMDLINE=$2
FVP_BASE='/home/lb/DS-5_v5.29.0/sw/models/bin' 
set -x
$FVP_BASE/FVP_Base_AEMv8A \
   --plugin $FVP_BASE/ScalableVectorExtension.so \
   -C SVE.ScalableVectorExtension.veclen=$VECLEN \
   --quiet \
   --stat \
   -C cluster0.NUM_CORES=1 \
   -C bp.secure_memory=0 \
   -C SVE.ScalableVectorExtension.enable_at_reset=1 \
   -C bp.refcounter.non_arch_start_at_default=1 \
   -C cluster0.cpu0.semihosting-use_stderr=1 \
   -C bp.vis.disable_visualisation=1 \
   -C cluster0.cpu0.semihosting-cmd_line="$CMDLINE"  \
   -a cluster0.cpu0=$CMDLINE
